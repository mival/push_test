function shareButton(options) {
    //message, subject, fileOrFileArray, url, successCallback, errorCallback
    var successCallback = function(shared) {
        console.log('share plugin:shared ->'+shared);
    }

    var errorCallback = function(data) {
        console.error('share plugin:error ->'+data);
    }

    if (window.plugins && window.plugins.socialsharing) {
        window.plugins.socialsharing.share(
            options['message'] || null, options['subject'],
            options['fileOrFileArray'] || null,
            options['url'] || null,
            options['successCallback'] || successCallback, options['errorCallback'] || errorCallback
        )
    }
}